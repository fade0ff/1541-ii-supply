Plug-in power supply for the Commodore 1541-II

See http://lemmini.de/1541-II-Supply/1541-II-Supply.html

---------------------------------------------------------------
Copyright 2021 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.